import re


# Напишите функцию для парсинга номерных знаков автомоблей Украины (стандарты - AА1234BB, 12 123-45AB, a12345BC)
# с помощью регулярных выражений. Функция принимает строку и возвращает None если вся строка не
# является номерным знаком. Если является номерным знаком - возвращает саму строку.
def parsing_car_number(number: str):
    """Сверяет номера машин с утвержденным шаблоном P.S. Использовал латинскую раскладку"""
    if isinstance(number, str):
        car_number = number
        template = r'^[A-Z]{2}[\d]{4}[A-Z]{2}$|^[\d]{2} [\d]{3}-([\d]{2}[A-Z]{2})$|^[a-z][\d]{5}[A-Z]{2}$'
        res = re.search(template, car_number)
        return res
    else:
        print('Exception')


car = parsing_car_number('12 123-45AB')
print(car)


# * Напишите класс, который выбирает из произвольного текста номерные знаки и возвращает их в виде пронумерованного
# списка с помощью регулярных выражений.


class NumberPlates:
    number_template = ''
    some_text = ''

    def __init__(self, template, text):
        if isinstance(template, str) and isinstance(text, str):
            self.number_template = template
            self.some_text = text
        else:
            print('Exception')

    def find_car_numbers(self):
        text = []
        res = re.findall(self.number_template, self.some_text)
        for count, items in enumerate(res):
            text.append((count+1, items))
        return text


str2 = "Ты одна такая на других не пох32 324-45ABожа,это как сравнить BMW 67 324-45BC и запороже," \
       "Ты предназначена лишь для меня," \
       "не для него ты для меня как солнце при езде на Cabrio CC5564GB твой силуэт прекрасен в темноте при свете фар," \
       "ты элегантна и опасна, как Jaguar c54321PP"
str1 = r'[A-Z]{2}[\d]{4}[A-Z]{2}|[\d]{2} [\d]{3}-[\d]{2}[A-Z]{2}|[a-z][\d]{5}[A-Z]{2}'
number_plates = NumberPlates(str1, str2)
print(number_plates.find_car_numbers())

# ** Создайте репозиторий на GitLab или GitHub. Сохраните отдельной веткой (пусть будет HW14) дз по регулярным
